package ru.ckateptb.lazystorage.storage.text.json;

import lombok.SneakyThrows;

public abstract class JsonEntity {
    @SneakyThrows
    @Override
    public String toString() {
        return JsonStorage.GSON.toJson(this);
    }
}
