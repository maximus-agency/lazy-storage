package ru.ckateptb.lazystorage.storage.text;

import ru.ckateptb.lazystorage.storage.Storage;

public interface TextStorage extends Storage {
    void load();

    void save();
}
