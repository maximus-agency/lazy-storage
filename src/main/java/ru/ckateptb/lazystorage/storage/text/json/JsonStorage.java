package ru.ckateptb.lazystorage.storage.text.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.SneakyThrows;
import ru.ckateptb.lazystorage.storage.text.TextStorage;
import ru.ckateptb.lazystorage.util.FileUtils;

import javax.annotation.PostConstruct;
import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class JsonStorage implements TextStorage {
    static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    @PostConstruct
    public void init() {
        load();
    }

    @SneakyThrows
    public void load() {
        Path path = file();
        if (!Files.exists(path)) {
            save();
            return;
        }
        File file = path.toFile();
        Class<? extends JsonStorage> clazz = this.getClass();
        try (Reader reader = new FileReader(file)) {
            Object obj = GSON.fromJson(reader, clazz);
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                field.set(this, field.get(obj));
            }
        }
    }

    @SneakyThrows
    public void save() {
        Path path = file();
        File file = path.toFile();
        FileUtils.forceMkdirParent(file);
        try (Writer writer = new FileWriter(file)) {
            GSON.toJson(this, writer);
        }
    }
}
