package ru.ckateptb.lazystorage.storage;

import java.nio.file.Path;

public interface Storage {
    Path file();
}
