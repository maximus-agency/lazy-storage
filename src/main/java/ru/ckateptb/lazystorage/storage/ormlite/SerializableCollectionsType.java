package ru.ckateptb.lazystorage.storage.ormlite;

import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.SerializableType;

import java.lang.reflect.Field;
import java.util.Collection;

public class SerializableCollectionsType extends SerializableType {
    private static SerializableType singleton;

    public SerializableCollectionsType() {
        super(SqlType.SERIALIZABLE, new Class<?>[0]);
    }

    public static SerializableType getSingleton() {
        return SerializableType.getSingleton();
    }

    @Override
    public boolean isValidForField(Field field) {
        return Collection.class.isAssignableFrom(field.getType());
    }
}
