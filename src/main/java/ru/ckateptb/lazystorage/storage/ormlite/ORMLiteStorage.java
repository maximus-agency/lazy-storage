package ru.ckateptb.lazystorage.storage.ormlite;

import com.j256.ormlite.jdbc.DataSourceConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.SneakyThrows;
import ru.ckateptb.lazystorage.storage.Storage;
import ru.ckateptb.lazystorage.util.DataSourceUtils;

@Getter
public abstract class ORMLiteStorage implements Storage, AutoCloseable {
    protected final ConnectionSource connection;

    @SneakyThrows
    public ORMLiteStorage() {
        HikariDataSource dataSource = DataSourceUtils.createDefaultSQLiteHikariDataSource(file());
        this.connection = new DataSourceConnectionSource(dataSource, dataSource.getJdbcUrl());
    }

    @Override
    public void close() throws Exception {
        connection.close();
    }
}